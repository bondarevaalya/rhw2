import React from 'react';
import './button.scss';

export class Button extends React.Component{
    constructor(props){
     super(props)
     this.props = props
    }
    render(){
        return(
            <button className={this.props.class} onClick={this.props.onClick}>{this.props.text}</button>
        )
    }
}